var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var imagemin = require('gulp-imagemin');
var replace = require('gulp-replace');
var fs = require('fs');
var inject = require('gulp-inject');
runSequence = require('gulp4-run-sequence');


//Watch and rebuild changes from scss,js and index
gulp.task('watch-build', function(done) {

    gulp.watch('../scss/style.scss').on('change',()=>{
        runSequence('sass',browserSync.reload);
        
    });

    gulp.watch('../js/app.js').on('change',()=>{
        runSequence('scripts',browserSync.reload);
    });

    gulp.watch('../index.html').on('change',browserSync.reload);


    (gulp.series('sass','scripts','imageMin','CSS')());
    done();

  });

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: '../'
        },
        port: 6455
    });
})

gulp.task('start', gulp.series('watch-build','serve'));


//--------------------    codigo para traspilar sass -> css

gulp.task('sass', function () {
    return gulp.src('../scss/style.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest('../dist/css/'));
});

//--------------------    codigo para traspilar varios js -> un js
gulp.task('scripts', function () {
    return gulp.src(['../js/*.js'])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('../dist/js'));
});

//--------------------    codigo para minificar imagenes

gulp.task('imageMin', () => gulp.src('../img/**.!(db)')
    .pipe(imagemin())
    .pipe(gulp.dest('../dist/img')));







//--------------------     REvisar - codigo para inyectar

gulp.task('CSS', () => {
    return gulp.src('../index.html')
        .pipe(replace(/<link href="([^\.]\.css)"[^>]*>/g, function (s, filename) {
            var style = fs.readFileSync(filename, 'utf8');
        }))
        .pipe(gulp.dest('../dist'));
});


gulp.task('JS', () => {
    return gulp.src('../index.html')
        .pipe(replace(/<script src="([^\.]\.js)"[^>]*>/g, function (s, filename) {
            var style = fs.readFileSync(filename, 'utf8');
        }))
        .pipe(gulp.dest('../dist'));
});



//--------------------     REvisar - codigo para inyectar
